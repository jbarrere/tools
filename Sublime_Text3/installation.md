## snippets
### Installation  
copy to `%APPDATA%Roaming\Sublime Text 3\Packages\User\snippets`.
(or using `Preferences` / `Browse Packages` menu).

## themes
- Cobalt2


### Installation
Menu `Preferences` / `Browse Packages`, create Colorsublime-Themes folder, and copy `Theme - Cobalt2.sublime-package`. 

## plugins 
- Package Control
- BracketHighlighter
- Pretty JSON
- Sublime-Better-Completion
- Better Completion
